package com.jlngls.progressbar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {

    private ProgressBar progressBarHorizontal;
    private ProgressBar progressBarCircular;
    private Button button;
    private int progresso = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBarHorizontal = findViewById(R.id.progressBarHorizontal);
        progressBarCircular = findViewById(R.id.progressBarCircular);
        button = findViewById(R.id.buttonID);

        //Sempre deixar o Progresso Circula invisivel pela linguagem de programação
        progressBarCircular.setVisibility(View.GONE);
    }

    public void onClick(View view){

        this.progresso = this.progresso + 1;
        progressBarHorizontal.setProgress(this.progresso);

        progressBarCircular.setVisibility(View.VISIBLE);

        //Acrescentei o For (izzi demais)
        for(int C=0; C < 10; C++){
            button.setText("Carregando");
        }

        if(this.progresso ==10) {
            progressBarCircular.setVisibility(View.GONE);
            this.progresso = 0;
            button.setText("Concluido");
        }


    }
}
